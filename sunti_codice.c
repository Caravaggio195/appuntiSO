/*	Sunti C		*/





/*		Monitor con n Conferenza		*/

//se arrivi in ritardo non parli più e restitusce False

Monitor conf{
 
	boolean called[N_SPEAKERS];
	condition ok2speak[N_SPEAKERS];
	condition ok2call;

 conf(int N_SPEAKERS){							//<--- prima cosa da fare
						
		for(int i = 0; i < N_SPEAKERS; i++) called[i] = false;
	}

	procedure_entry boolean chiama(int chiamato){
		ok2call.wait();
		if(ok2speak[chiamato].signal()) {
			called[nome] = true;
			return true;
		}else return false;
	}
 
	procedure_entry boolean arrivato(int nome){
		if(called[nome]) return false;
		else ok2speak[nome].wait();
		return true;
	}
 
	procedure_entry void finepresentazione(int nome){		//<--- funzione aspetta fine
		ok2call.signal();
	}
 
	
 
}




























/*		Monitor con coda di attesa		*/

//	prestare particolare attenzione al waitingReaders, waitingWriters 

// scitto con python

monitor lwlrbb:
 
	Queue[generic_type] buffer
	int  waitingReaders, waitingWriters
	condition ok2write, ok2read 
	boolean exitNull;
	int max;
 
	lwlrbb (int max):
		self.max = max
		buffer = new List(max)
		waitingReaders = 0
		waitingWriters = 0
		exitNull = False
 
	@entry
	void write(generic_type val): 
		if (buffer.size()>=MAX) :
				if (waitingWriters>Max):
					buffer.dequeue() 		//discard element
					ok2write.signal()
			waitingWriters+=1
			ok2write.wait()
			waitingWriters-=1
		buffer.enqueue(val)
		if (waitingReaders>0) ok2read.signal()
		else if (waitingWriters>0) ok2write.signal()
		
	@entry
	generic_type read():
		if (buffer.isEmpty(): 
			if (waitingReaders>Max):
				exitNull = True				//set flag to make the woke up reader return null
				ok2read.signal()
			waitingReaders++
			ok2read.wait()
			waitingReaders--
			if (exitNull):
				exitNull = False
				return null
		element = buffer.dequeue()
		if (waitingWriters>0) ok2write.signal()
		else if (waitingReaders>0) ok2read.signal()































/*		Esame 2017.05.29 C1		*/	

// programma rubbabandiera	

Monitor rb () {
 
  condition ok2chiama, ok2run[MAX], ok2fine;
  int npronti, punteggio[2], arrivati, c
 
 procedure entry void nuovapartita() {
    c = npronti = arrived punteggio[0] = punteggio[1] = 0;
}
 procedure entry int chiama(int *chiamati) {
     c = chiamati.length;
     if(npronti < 2 * MAX)
           ok2chiama.wait();
     for(int i = 0; i < c; i++){
           ok2run[i].signal();
           ok2run[i].signal();
     		}
     ok2fine.wait() // chiama tutti i processi e li fa finire se il gioco è finito 
     if(max(punteggio) == 10)
           for(int i = 0; i in range(MAX); i++){
                ok2run[i].signal();
                ok2run[i].signal();
          }  
      return punteggio;
}
 
 procedure entry int pronto(int squadra, int numero) {
      if(max(punteggio) == 10)
            return -1;
      npronti++;
      if (npronti > 2 * MAX){
            ok2chiama.signal();
            if(n not in chiamata)
                   ok2run[n].wait();
			}
       npronti--;
       if(max(punteggio) == 10)
                return -1;
       else return 0;
}
 
 procedure entry void allabandiera(int squadra, int numero) {
      arrived++; 
      if(arrived = 2 * c){
                 punteggio[1 - squadra]++;
                 ok2fine.signal();  //li fa correrer tutti una volta dato il punto
 			}
 }
}




























/*		Esame 21 - 06 2013			*/

// C1

#DEFINE N
#DEFINE NELEM

monitor nvie{
	condition okput, okget;
	Queue buffer[N];
	boolean pieno[N];// <-----inizializzati tutti con valore False

	procedure_entry put (int n, generic_type object){
		if(buffer[n].length()>= NELEM){
			okput.wait();
		}
		pieno[n]= True;
		buffer[n].enqueue(object);
		okget.signal();

	}


	procedure_entry get (generic_type *object){
		int n-pieni;
		for(int i; i<=N ; i++){
			if(pieno[i]==True){n-pieni++;}
		}
		if(n-pieni<=(N/2)){
			okget.wait();
		}
		for(int d;d<=N;d++){
			if(buffer[d].length()==0){object[d]=NONE;}
			else
			object[d]=buffer[d].dequeue();
			if(buffer[d].length()==0){pieno[d]=False;}
		}
		okput.signal();
	}
}


/*		server Stampa		*/

pid index[N] /*index contiene gli id dei vari server*/
 
process server[0]
{
	while(1)
	{
		tmp msg=arecv(*);
		for (i=1;i<N;i++)
			asend(index[i],tmp);
		print(tmp);
	}
}
 
process server[1...N-1]
{
	while(1)
	{
		msg tmp =arecv(*)
		if (tmp.sender==index[0])
			print(tmp);
		else
			asend(index[0],tmp);
	}
}



