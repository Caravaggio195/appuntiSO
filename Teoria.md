# Teoria

Riporterò le domande di Teoria.

### Lista Esami:

* [Esame_24_05_2012](#esame_24_05_2012-link)
* [Esame_15_06_2012](#esame_15_06_2012-link)
* [Esame_16_07_2012](#esame_16_07_2012-link)
* [Esame_18_09_2012](#esame_18_09_2012-link)
* [Esame_24_01_2013](#esame_24_01_2013-link)
* [Esame_14_02_2013](#esame_14_02_2013-link)
* [Esame_21_06_2013](#esame_21_06_2013-link)
* [Esame_19_07_2013](#esame_19_07_2013-link)
* [Esame_12_09_2013](#esame_12_09_2013-link)
* [Esame_22_01_2014](#esame_22_01_2014-link)
* [Esame_21_02_2014](#esame_21_02_2014-link)
* [Esame_03_06_2014](#esame_03_06_2014-link)
* [Esame_24_09_2014](#esame_24_09_2014-link)
* [Esame_14_02_2015](#esame_14_02_2015-link)
* [Esame_21_06_2018](#esame_21_06_2018-link)

## Esame_24_05_2012 [Link](http://www.cs.unibo.it/~renzo/so/compiti/2012-05-24.tot.pdf)

1.  **G1:**

2.  **G2:**
In un sistema multitasking un processo chiede una operazione di I/O (ad esempio una lettura di un
dato da disco tramite la chiamata posix “read”). Raccontare con il maggior numero di particolari possibili la sequenza di azioni compiute dal sistema operativo e dal fino al completamento dell'operazione di I/O.

* **Risposta :**

Immagine che spiega gli stati di un processo.
![immagine che spiega il ciclo di un processo.](http://so.v2.cs.unibo.it/wiki/images/f/f7/Dds.png)

In un sistema multitasking vengono eseguiti più processi contemporaneamente.
esistono due tipi principali:

1. **Senza prelazione (cooperative):**
      Non ha bisogno di supporto hardware. Preleva un processo per eseguirlo e poi lo lascia andare finché non si blocca (per I/O o in attesa di un altro processo) oppure non lascia volontariamente la CPU.

2.  **Con prelazione (preemptive):**
    Ha bisogno di alcuni componenti hardware. Prevedere che un processo sia in esecuzione per un tempo stabilito, poi viene sospeso e lo scheduler prende un'altro processo.

Prenderemo in esame lo scheduler di tipo preemptive dove con un operazione  I/O il processo viene "bloccato" o messi in attesa, aspettando l'arrivo dei dati necessari, permettendo agli altri processi di usare la CPU, viene messo in attesa su una coda di processi e verrà sbloccato solo quando l'algoritmo di scheduling lo riterrà opputuno.

## Esame_15_06_2012 [Link](http://www.cs.unibo.it/~renzo/so/compiti/2012-06-15.tot.pdf)

1.  **G1:**
Sia dato questo programma:
```
        Program P:
        for (i=0;i<2;i++) {
        long_compute();
        io_on_dev(i);
        }
        short_compute();
```
`long compute` impiega 6 ms, `short_compute` 2 ms e `io_on_dev` impiega 7ms. (l'indice della funzione `io_on_dev indica` il device sul quale viene eseguita la funzione). Considerando in un sistema di elaborazione dove sono in esecuzione tre istanze del programma P che sono state attivate ai tempi 0,3ms e 7ms e che il sistema usa uno scheduler round robin per l'accesso alla CPU disegnare il
diagramma di Gannt dell'esecuzione. (time slice=2 ms).

2. **G2:**
Rispondere ad almeno due delle seguenti domande:
 * cosa e' e come funziona un sistema RAID?
 * cosa e' il trashing e quali metodi esistono per evitare il verificarsi della situazione di trashing?
 * cosa sono il linking dinamico e il loading dinamico?

* **Risposta:**
 1.  **RAID**, acronimo di **"Redundant Array of Independent Disks"** , è una tecnica di archiviazione dati, l'idea di RAID è quella di installare un contenitore con più dischi, sostiure il controller dei dischi con un controller RAID così verrebbe visto dal sistema operativo come un disco singolo. La ridondanza dei dati e di parallelismo nel loro accesso garantisce tolleranza ai guasti e quindi migliore affidabilità. Le modalità più diffuse sono RAID 0, 1, 5 e 10.
 2. ci sono due spiegazioni:
        * **Il trashing** è una delle tecniche utilizzate da hacker e cracker per reperire informazioni da utilizzare negli attacchi informatici.
       * **Thrashing, ovvero spendere più tempo per la paginazione che per l'esecuzione**, il sistema operativo potrebbe erroneamente essere indotto a dedurre che sia necessario aumentare il grado di multiprogrammazione. In questo modo vengono avviati nuovi processi che però, a causa della mancanza di frame liberi, cominceranno a loro volta ad andare in thrashing: **in breve le prestazioni del sistema collassano fino ad indurre l'operatore a dover terminare forzatamente alcuni processi.** Un modo per limitare questo fenomeno consiste nel utilizzare una procedura di rimpiazzamento locale, ovvero dare la possibilità al gestore della memoria virtuale di sostituire le pagine associate al solo processo che ne fa richiesta. In questo modo si impedisce che l'intero sistema vada in thrashing.
 3. * **linking dinamico:** La risoluzione di simboli non definiti rimandata fino a che il programma non è in esecuzione. Lanciando il programma per l'esecuzione vengono caricati questi oggetti / librerie e si stabilisce il collegamento finale.
    * **loading dinamico:** È possibile scrivere un programma che decide quali classi caricare ed utilizzare rendendo, di conseguenza, il comportamento a runtime completamente dinamico.

## Esame_16_07_2012 [Link](http://www.cs.unibo.it/~renzo/so/compiti/2012-07-16.tot.pdf)

1.  **G1:**
Scrivere una stringa di riferimenti infinita per la quale gli algoritmi MIN, LRU e FIFO si comportino
esattamente allo stesso modo se applicati ad una memoria di 3 frame. La stringa deve fare riferimento a un numero
finito di pagine e deve generare un numero infinito di page fault.
2.  **G2:**
Rispondere ad almeno due delle seguenti domande:
 *  A cosa serve l'algoritmo di Dekker e come funziona?
 *  Access Control List/Capability, a cosa servono? Quali sono le differenze?
 *  Cosa significa “partizionare” un disco? A cosa serve?

* **Risposta:**

 **2.1** L'algoritmo di **Dekker** è una soluzione per la mutua esclusione e sincronia fra processi, impedendo deadlock, facendo entrare solo un processo per volta nella sezzione critica.

 **2.2** Entrambe risolvono il problema della protezione di oggetti (file) e Processi(utenti).
 immaginamio una tabella così formata.

| Dominio| File1    | File 2 | File3 | Stampante | Dominio 2 |
|--------|-------|-----------|-------|-----------|-----------|
| 1      | Read  | Read+Write|        | Write    | Enter|
| 2      |   Read+Write|           |Read+Write+Execute| |

   * Le **ACL** sono utilizzate per la determinazione dei permessi riguardanti oggetti (file e cartelle) memorizzati sui dischi.Dove F1 è un file e ACL è rappresentata come una lista di permessi sugli oggetti.

esempio:
```
        [F1]→[1:R; 2:RW]
        [F2]→[1:RW]
        [F3]→[2:RWX]
```        
   * La lista delle **Capability(C-list)** invece associa a ciascun processo un elenco di quali operazioni sono permesse.

esempio:
```
       [P1]             [P2]
        ↓                ↓
        [F1:R]          [F1:RW]
        [F2:RW]         [F3:RWX]
```

 **2.3**  Con il termine **partizionare** si indica l'azione di divide logicamente un disco fisico in più file system separati. Gli HD vengono partizionati per diversi motivi:
  * **Usi differenti**: diversi file system, area di swap, altri S.O.. . .
  * **Robustezza**: se una partizione si danneggia, può essere riformattata senza modificare le altre
  * **Confinamento**: i file non possono superare i confini della partizione
  * Maggiore velocità al reboot e al backup

## Esame_18_09_2012 [Link](http://www.cs.unibo.it/~renzo/so/compiti/2012-09-18.tot.pdf)

1.  **G1:**
 Sia dato un file system di tipo UNIX. Puo' essere soggetto a diversi tipi di incoerenza. Per ogni tipo
indicare come riconoscere l'incoerenza e le azioni correttive operate da fsck.
  * errato numero di blocchi del file
  * errato numero di link di un file (reference count)
  * errori nella tabella/bitmap dei blocchi occupati/liberi
  * problemi di connettivita' del file system

 * **Risposta:**
fsck (file system check) è un comando che si occupa del controllo della coerenza dei dati presenti in un file system ( [Link1 ](https://linux.die.net/man/8/fsck) , [link2](https://users.dimi.uniud.it/~ivan.scagnetto/SO4/filesystem3.pdf))

2.  **G2:** stesse domande G2 [Esame_16_07_2012](#esame_16_07_2012-link)

## Esame_24_01_2013 [link](http://www.cs.unibo.it/~renzo/so/compiti/2013-01-24.tot.pdf)
1.  **G1:**Considerare i seguenti processi gestiti mediante uno scheduler round robin con timeslice di 3ms su una macchina SMP a due processori: l'Input-Output avviene su un'unica unita'.

        P1: cpu 5 ms; I-O 4 ms; cpu 2 ms; I-O 4 ms; cpu 5 ms
        P2: cpu 2 ms; I-O 4 ms; cpu 2 ms; I-O 1 ms; cpu 5 ms
        P3: cpu 4 ms; I-O 3 ms; cpu 3 ms; I-O 2 ms; cpu 3 ms
        P4: cpu 13 ms; I-O 1 ms


  * **Risposta:**
-- Nono so se giusto --

|Tempo|1|2|3|4|5|6|7|8|9|10 | 11|12|13|14|15|16|17|18|19|20|21|22|23|24|
|--|--|-|-|-|-|-|-|-|-|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|
|CPU1|P1|P1|1|4|4|4|3|2|2| | |1|1| |3|3|3|4| |1|1|1|1|1|
|CPU2|P2|P2|3|3|3|1|1|4|4|4|4|4|4|4|4|4|2|2|2|2|2|3|3|3|
|I/O|| | |P2|2|2|2| |1|1|1|1|3|3|3|2|1|1|1|1|3|3|4| | |
2.  **G2:**
 Nel corso abbiamo incontrato concetti che possono apparire simili, o diversi approcci di soluzione per lo stesso
problema.
Il candidato scelga due dei tre confronti proposti e indichi per ognuno: in quale ambito dei sistemi operativi si
applicano i concetti espressi, quali sono i punti in comune e quali le differenze fra gli elementi da confrontare.
  * Confrontare la paginazione e la segmentazione di memoria
  *  Confrontare l'allocazione concatenata e l'allocazione indicizzata per file system
  *  Confrontare microkernel e kernel monolitici

* **Risposta:**

 1. paginazione **VS** segmentazione:
    * **paginazione di memoria**: Lo spazio degli indirizzi virtuali è suddiviso in unità di dimensione fissa, chiamate pagine, offre il vantaggio di mettere in memoria solo ciò che serve.
    * **segmentazione di memoria**: Lo spazio di memoria è diviso in segmenti, differenzia dalla paginazione perchè le pagine hanno lughezza fissa mentre i segmenti no, ha il vantaggio di creare moduli logicamente coesi, in questo modo si riducono i tempi di I/O da disco. La suddivisione in segmenti e' compito del programmatore;
Esistono però alcuni metodi misti (MULTICS) che utilizza segmenti che poi vengono paginati.

 2. allocazione concatenata **VS** allocazione indicizzata:
    * **Allocazione Contigua:**Ogni file occupa un insieme contiguo di blocchi sul disco.
    * **Allocazione Concatenata:** Ogni file è gestito tramite una lista concatenata di blocchi di disco: i blocchi non devono essere necessariamente contigui e quindi possono stare in punti diversi del disco.
    * **Allocazione Indicizzata:** Mantiene tutti i puntatori ai blocchi dei file in un’unica struttura: il blocco indice.

 3. kernel monolitici **VS** microkernel:
    * I **kernel monolitico:** l'intero sistema viene eseguito come un unico programma in modalità kernel, il sistema viene scritto come una raccolta di procedure linkate fra di loro all'interno di un unico eseguibile.
    * I **microkernel:** Questo tipo di approccio invece punta ad essere più stabile possibile, infatti se noi suddividiamo tutto il sistema in piccoli moduli e dove solo uno esegue in modalità kernel, mentre gli altri vengono eseguiti come semplici processi e separati, un mal funzionamento o errore farà bloccare solo il singolo processo e non l'intero sistema


## Esame_14_02_2013 [Link](http://www.cs.unibo.it/~renzo/so/compiti/2013.02.15.tot.pdf)

1.  **G1:**
 Considerare i seguenti processi gestiti mediante uno scheduler round robin con timeslice di 4ms su una
macchina SMP a due processori:l'Input-Output avviene su un'unica unita'.

        P1: cpu 5 ms; I-O 4 ms; cpu 2 ms; I-O 5 ms; cpu 5 ms
        P2: cpu 2 ms; I-O 4 ms; cpu 2 ms; I-O 1 ms; cpu 5 ms
        P3: cpu 3 ms; I-O 3 ms; cpu 3 ms; I-O 1 ms; cpu 3 ms
        P4: cpu 13 ms; I-O 1 ms

2.  **G2:**stesse domande G2 [Esame_24_01_2013](#esame_24_01_2013-link)

## Esame_21_06_2013 [Link](http://www.cs.unibo.it/~renzo/so/compiti/2013.06.21.tot.pdf)

1.  **G1:**
 Siano dati i processi P1, P2 e P3 in ordine descrescente di priorita' (P1 ha massima priorita' e P3 minima)

        P1: CPU 2ms, I/O 2ms, CPU 2ms, I/O 2ms, CPU 2ms
        P2: CPU 3ms, I/O 2ms, CPU 3ms, I/O 2ms, CPU 3ms
        P3: CPU 1ms, I/O 2ms, CPU 1ms, I/O 2ms, CPU 1ms
I tre processi usano unita' indipendenti per l'I/O (non c'e' contesa per l'I/O)
Si calcoli il diagramma di Gannt (descrivendo il procedimento) per uno scheduler a priorita' di tipo preemptive e lo si confronti con uno scheduler a priorita' di tipo non preemptive.
PREEMPTIVE:

|TMP |1 |2  |3  |4  |5  |6  |7  |8  |9  |10 |11 |12 |13 |14 |15 |16 |17 |18 |
|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|
|CPU|  1|  1|  2|  2|  1|  1|  2|  3|  1|  1|  2|  2|  2|  3|  2|  2|  2|  3|
|frst| 2|  2|   |   |  2|  2|   |   |   |2  |3  |  3|  3|   |   |   |  3|   |
|second|3| 3|  3|  3|  3|  3|  3|   |   |   |   |   |   |   |   |   |   |   |
|IO1 |  |   |  1|  1|   |   |  1|  1|   |   |   |   |   |   |   |   |   |   |
|IO2 |  |   |   |   |   |   |   |  2|2  |   |   |   |   |2  |2  |   |   |   |
|IO3 |  |   |   |   |   |   |   |   |3  |3  |   |   |   |   |3  |3  |   |   |

NON PREEMPTIVE:

|TMP |1 |2  |3  |4  |5  |6  |7  |8  |9  |10 |11 |12 |13 |14 |15 |16 |17 |18 |19 |20 |21 |22 |
|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|
|CPU|  1|  1|  2|  2|  2|  1|  1|  2|  2|  2| 1 |  1|  2|  2|  2|  3|   |   | 3 |   |   | 3 |
|frst| 2|  2|   |   |  1|   |   |   |   | 1 |   |  3|  3|   |   |   |   |   |   |   |   |   |
|second|3| 3|  3|  3|  3|  3|  3| 3 |   | 3 | 3 |   |   |   |   |   |   |   |   |   |   |   |
|IO1 |  |   |  1|  1|   |   |   |  1| 1 |   |   |   |   |   |   |   |   |   |   |   |   |   |
|IO2 |  |   |   |   |   | 2 | 2 |   |   |   | 2 | 2 |   |   |   |   |   |   |   |   |   |   |
|IO3 |  |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   | 3 | 3 |   | 3 | 3 |   |



2.  **G2:**
 Si risponda alle seguenti domande:
  * quali sono le differenze fra la paginazione e la segmentazione nella gestione della memoria?
  * quali sono le differenze fra le operazioni P e V dei semafori e le operazioni wait/signal delle variabili di condizione?
  * allocazione contigua, concatenata e indicizzata nei file system, quali sono le differenze e i campi di applicazione?

* **Risposta:**

 1. paginazione **VS** segmentazione:
    * **paginazione di memoria:** Lo spazio degli indirizzi virtuali è suddiviso in unità di dimensione fissa, chiamate pagine, offre il vantaggio di mettere in memoria solo ciò che serve.
   * **segmentazione di memoria:** Lo spazio di memoria è diviso in segmenti, differenzia dalla paginazione perchè le pagine hanno lughezza fissa mentre i segmenti no, ha il vantaggio di creare moduli logicamente coesi, in questo modo si riducono i tempi di I/O da disco. La suddivisione in segmenti e' compito del programmatore.
Esistono però alcuni metodi misti (MULTICS) che utilizza segmenti che poi vengono paginati.

 2. Operazioni **`P/V`** **VS** **`wait/signal`**:
    * P/V : sono operazioni su semafori (un semaforo è inizializzato con un valore `int` positivo).
      - `P`: Il semaforo viene decrementato. Se, dopo il decremento, il semaforo ha un valore negativo, il task viene sospeso e accodato, in attesa di essere riattivato da un altro task.
      - `V` : Il semaforo viene incrementato. Se ci sono task in coda, uno dei task in coda viene tolto dalla coda e posto in stato di ready.
    * wait/signal:
      - `wait`: applicata ad una variabile condizionale, permette di sospendere un processo che occupa il monitor, facendo in modo che il processo sparisca temporaneamente dal monitor e venga posto in una coda d'attesa per quella variabile condizionale.
      - `signal` :  risveglia esattamente un processo sospeso sulla variabile condizionale per cui è chiamata; questo processo riprende la propria esecuzione.

 3. differenze pro e contro fra **contigua, concatenata e indicizzata** :
    * Nell'**allocazione contigua** ogni file viene allocato in blocchi di memoria adiacenti.
      - PRO: non sono presenti strutture di collegamento, l'accesso sequenziale è efficiente. Anche l'accesso diretto risulta efficiente.
      - CON: frammentazione esterna, politica scelta blocchi liberi da usare; i file non possono crescere
      - APP: e'utilizzato nell'ISO 9660 (dischi ottici) e nel file system NTFS.
    * Nell'**allocazione concatenata** ogni file è una lista concatenata di blocchi ed ogni blocco contiene un puntatore al blocco successivo.
      - PRO: accesso sequenziale o in append mode efficiente, risolve la frammentazione esterna
      - CON: l'accesso diretto è inefficiente, se il blocco è piccolo l'overhead per i puntatori può essere rilevante.
      - APP: utile per i log, dato l'accesso efficiente in append mode. Usata in Xerox alto, DEC TOPS-10
    * Nell'**allocazione indicizzata** l'elenco dei blocchi che compongono un file viene memorizzato in un'area indice, quindi per accedere ad un file si carica in memoria la sua area indice e si utilizzano i puntatori contenuti.
      - PRO: risolve frammentazione esterna; efficiente per accesso diretto. Il blocco indice viene caricato in memoria solo quando il file è aperto.
      - CON: la dimensinoe del blocco indice determina ampiezza massima file
      - APP: file grosse dimensioni in quanto e' facile accedere alla parte intermedia del file. Utile per i file system di uso generale.

## Esame_19_07_2013 [Link](http://www.cs.unibo.it/~renzo/so/compiti/2013.07.19.tot.pdf)

1.  **G1:**
Siano dati i processi P1, P2 e P3 in ordine descrescente di priorita' (P1 ha massima priorita' e P3 minima).

        P1: CPU 2ms, I/O 1ms, CPU 2ms, I/O 2ms, CPU 2ms
        P2: CPU 3ms, I/O 2ms, CPU 3ms, I/O 2ms, CPU 3ms
        P3: CPU 1ms, I/O 1ms, CPU 1ms, I/O 1ms, CPU 1ms
I tre processi usano la stessa unita' per l'I/O (le richieste di I/O vengono gestite in ordine FIFO)
Si calcoli il diagramma di Gannt (descrivendo il procedimento) per uno scheduler round robin con time slice 2ms.
* **Risposta:**

|TMP |1 |2  |3  |4  |5  |6  |7  |8  |9  |10 |11 |12 |13 |14 |15 |16 |17 |18 | 19|
|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|
|CPU|  1|  1|  2|  2|  1|  1|  2|  3|  1|  1|  2|  2|  2|  3|   |2  | 2 | 2 |  3|
|frs|  2|  2|   |   |  2|  2|   |   |   |   |   |  3|  3|   |   |   |  3| 3 |   |
|sec|  3|  3|  3|  3|  3|  3|  3|   |   |   |   |   |   |   |   |   |   |   |   |
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |
|FIO|   |   |   |   |   |   |   |  2|  3|  3|   |   |   |   | 3 |   |   |   |   |
|IO |   |   |  1|  1|   |   |  1|  1|  2|  2| 3 |   |   | 2 |  2| 3 |   |   |   |

2.  **G2:**
 Le seguenti affermazioni sono vere o false? (Motivare dettagliatamente le risposte).
 * I processi vengono eseguiti piu' velocemente nei sistemi con memoria virtuale.
 * In un File System di tipo FAT non vi e' limite massimo alla dimensione di un singolo file.
 * In un File System di tipo ext2 non vi e' limite massimo alla dimensione di un singolo file.
 * I file system con allocazione contigua non vengono piu' utilizzati.
 * L'algoritmo di rimpiazzamento LRU non puo' essere utilizzato insieme alla tecnica del Working Set.

* **Risposta:**
 1. Falso , invece è il contrario perchè la mmu spreca tempo di clacolo.
 2. Falso la dimesione del singolo file è di massino 2^16 -1 bytes (circa 2GB).
 3. Falso la dimesione del singolo file è di massimo 4TB.
 4. Falso é utilizzato nell'ISO 9660 (dischi ottici) e nel file system NTFS.
 5.

## Esame_12_09_2013 [Link](http://www.cs.unibo.it/~renzo/so/compiti/2013.09.12.tot.pdf)  

1.  **G1:**
 Si consideri il seguente scenario per un banchiere multivaluta.
   * E' uno stato safe? Spiegate le motivazioni della vostra risposta.
   * In caso positivo, mostrate una richiesta di allocazione che porterebbe il sistema in uno stato unsafe.    In caso negativo, illustrate una richiesta di deallocazione che porterebbe il sistema in uno stato safe.

```
            Valuta 1                Valuta 2
            COH = 5                 COH = 25 Ci
            Ci  Pi     Ni           Ci    Pi    Ni
            Max Corr.Residuo        Max Corr. Residuo
        p1: 15  5     10            30     5    25
        p2: 12  6      6           100    65    35
        p3: 10  6      4            50    15    35
        p4: 7   2      2            20    10    10
        p5: 3   1      1            70    35    35
```

1.  **G2:**
 Le seguenti affermazioni sono vere o false? (Motivare dettagliatamente le risposte).
   * Il meccanismo RAID livello 0 aumenta la tolleranza ai guasti.
   * I microkernel consentono maggiori performance rispetto ai kernel monolitici
   * L'uso di librerie dinamiche consente una minor occupazione di memoria principale e secondaria.
   * Esistono stringhe di riferimenti per le quali LRU e MIN causano lo stesso numero di page fault.
   * Sia C la capability che consente l'accesso alla risorsa R. Per consentire che C sia memorizzata dal processo che usa R e' necessario che C sia crittata mediante un algoritmo a doppia chiave (Pubblica-Privata).

* **Risposta:**
1. Falso, il meccanismo RAID livello 0 non presenta meccanismi di ridondanza (blocchi di parità), da permettere  una tolleranza ai guasti.
2. Falso, perchè anche la comunicazione fra i vari moduli ha un costo in termini di tempo.
3. Vero, perchè con il linking dinamico le libbrerie sono condivise per tutti i programmi in esecuzione ed in questo modo la memoria principale è meno satura.
4. [LRU](https://it.wikipedia.org/wiki/Memoria_virtuale#Least_Recently_Used_(LRU)) ,
[MIN](http://www.cs.unibo.it/~sacerdot/so/lucidi/so-07-memoria-4p.pdf).
sì esistono [vedi qui](http://so.v2.cs.unibo.it/wiki/index.php?title=Prova_Teorica_2013.09.12).

**P.S. Stringa di riferimento  è la pagina che si vuole accedere.**

5. Falso, nelle criptogafie delle capability viene usata la criptografia simmetrica, poichè se salvate nello spazio utente non hanno bisogno di attraversare una rete.

## Esame_22_01_2014 [Link](http://www.cs.unibo.it/~renzo/so/compiti/2014.01.22.tot.pdf)

1.  **G1:**
Sia dato l'algoritmo di rimpiazzamento MAXNUM. Come pagina da rimpiazzare MAXNUM sceglie sempre quella con l'indirizzo logico piu' alto (numero di pagina maggiore).
    * mostrare una stringa di riferimenti di lunghezza infinita e che generi infiniti page fault tale che MIN e MAXNUM si comportino esattamente nello stesso modo
    * mostrare una stringa di riferimenti di lunghezza infinita tale che MAXNUM generi un page fault ad ogni accesso in memoria mentre MIN generi un numero finito di page fault
(in entrambi i punti l'insieme delle pagine utilizzate nelle stringhe di riferimenti deve essere finito)

* **Risposta:**
     * stringa di riferimento: 1 2 3 4 5 6 7 8 9 1 2 3 4 5 6 7 8 9...
     * stringa di riferimento: 1 2 3 4 5 3 4 3 5 3 4 5 ...

2.  **G2:**
Rispondere alle seguenti domande:
   * La paginazione non elimina completamente ne' la frammentazione interna ne' quella esterna ma le rende trascurabili. Perche'?
   * Il file system ext2 e' piu' efficiente nell'accesso diretto a file di piccole dimensioni rispetto a quelli di grandi dimensioni. Perche'?
   * In quali casi l'algoritmo C-Look e' preferibile all'algoritmo Look? Perche'?
   * Nella memorizzazione delle capability per il controllo di accesso ai file puo' essere usato un algoritmo di crittografia a singola chiave (chiave privata). Perche'?


## Esame_21_02_2014 [Link](http://www.cs.unibo.it/~renzo/so/compiti/2014.02.21.tot.pdf)

[Stesse Esame_22_01_2014 ](#esame_22_01_2014-link)


## Esame_03_06_2014 [Link](http://www.cs.unibo.it/~renzo/so/compiti/2014.06.03.tot.pdf)

1.  **G1:**
   Costruire un Grafo di Holt con 6 processi e 6 classi di risorse in modo che valgano contemporaneamente le seguenti
proprieta':
    * la situazione sia di Deadlock
    * non sia sufficente eliminare un processo perche' il Deadlock venga risolto
    * ogni processo abbia esattamente una richiesta in sospeso
    * il grafo sia connesso (ci sono archi fra nodi di partizioni diverse comunque si partizioni l'insieme dei nodi)

* **Risposta:**
Guarda [questo](http://www.math.unipd.it/~cpalazzi/files/CE01a%20-%20Sincronizzazione.pdf).
Devono esserre presenti:
    1. almeno 2 cicli ( in teoria potrebbero crearsi anche 3).
    2. Gli archi uscenti da ciascun processo è unico.
    3. collegare le risorse anche non ad dei cili ed assegnare un numero di risorse in modo da creare Deadlock.


2.  **G2:**
  Rispondere alle seguenti domande:
    * Quali sono le differenze fra un device driver di una unita' con funzionalita' di DMA e quello di una unita' senza DMA?
    * L'algoritmo del Banchiere e il grafo di Holt servono entrambi per gestire i Deadlock. Quali sono le differenze, quando e
perche' si usa l'uno e quando e perche' l'altro?
    * Quali problemi risolve la paginazione e quali la segmentazione?

* **Risposta:**


## Esame_24_09_2014 [Link](http://www.cs.unibo.it/~renzo/so/compiti/2014.09.24.tot.pdf)

1.  **G1:**

2.  **G2:**
    * il supporto DMA e' necessario per implementare un device driver di tipo interrupt driven? Perche'?
    * quale supporto hardware e' necessario per l'impementazione di uno scheduler round robin? Perche'?
    * quale supporto hardware e' necessario per implementare un algoritmo di rimpiazzamento FIFO? Perche'?

* **Risposta:**
 1.


## Esame_14_02_2015 [link](http://www.cs.unibo.it/~renzo/so/compiti/2015.02.14.tot.pdf)
1.  **G1:**

2.  **G2:**Rispondere alle seguenti domande:
    * Quali sono i casi di frammentazione interna ed esterna che possono avvenire usando la paginazione.
    * Per I file system di tipo fat non e' efficiente l'accesso diretto (ad esempio con la system call lseek) a file di grandi dimensioni.Perche'?
    *  Per quali tipi di processo e' indicato uno schedule a priorita' statica? Quale problema puo' essere causato da uno scheduler a priorita' statica? perche'?
    * Cosa e' un knot in un grafo? Quale teorema lega la definizione di knot in un grafo e la presenza di deadlock fra processi?

* **Risposta:**
    1. la paginazione può avere frammentazione:
     * **Interna:**Quando lo spazio allocato dal processo non è interamente utilizzato.
     * **Esterna:**Dopo un certo numero di allocazioni e deallocazioni, lo spazio appare diviso in aree piccole.

    2. Non essendo un file system di tipo seguenziale effettua un accesso al disco per ogni elemento, quindi con file di grandi dimesioni gli accessi a disco aumentano.

    3. utilizzando un scheduler a priorità statica attribusce ai processi una priorità quando sono creati.
     * **problemi:**processi a bassa priorità possono essere posti in starvation da
processi ad alta priorità.
     * **utilizzo:**vengono utilizzati nei processi server.
    4. Knot:
     * Un knot in un grafo grafo G è il massimo sottoinsieme (non banale) di nodi M tale che per ogni n in M, R(n)=M . In altre parole: partendo da un qualunque nodo di M, si possono raggiungere tutti i nodi di M e nessun nodo all'infuori di esso.
     * Detected Deadlock.
     **Teorema:**Dato un grafo di Holt con una sola richiesta sospesa per processo
se le risorse sono ad accesso mutualmente esclusivo, seriali e non prerilasciabili,
allora il grafo rappresenta uno stato di deadlock se e solo se esiste un knot.

## Esame_21_06_2018 [link](https://photos.google.com/share/AF1QipNx5KRWKsw7vKJIApkybk6mug7AaxHGANwudKs8tUSRPNC5f6fU5d_Om5ypdQxVGA/photo/AF1QipPnkjUssut9U-PTMb4Rn8-OAtIBabe_G8e7d0R8?key=MXpTRzE3a0RLZTE4TlhMWUJxSnkzSXdOellheGRB)
1.  **G1:**

* **Risposta:**
Tabella dei soldi ancora da richiedere dei vari clienti

|    |1  |2  |3  |
|--- |---|---|---|
|A   |  5|  9| 19|
|B   |  5|  9| 19|
|C   |  5|  9| 19|

Soldi del Banchiere:**`1=4,2=8,3=18`**

2.  **G2:**
  * A cosa serve e quando viene eseguito l'algoritmo di Working-Set?
  * Come si calcola la lunghezza massima di un file su file system di tipo FAT?
  * Quali sono le differenze fra visus e worm?
  * in quali casi la ready queue può essere vuota? sono casi patologici o fisiologici?

* **Risposta:**  
    1. Se l'ampiezza delle pagine è ben definita, il Working-set è una buona approssimazione della pagine utili.
       * **a cosa serve?** serve per controllare l'allocazione dei frame dei singoli processi.
       E controlla se all'allocazione di un processo non ci sono sufficenti frame in memoria.
    2. [vedi link](http://www.ntfs.com/ntfs_vs_fat.htm)
    3.
    * [Virus](https://it.wikipedia.org/wiki/Virus_(informatica)) informatici Si tratta di un tipo di programma malevolo in grado di replicarsi, in modo che possa spargersi tra i file di un computer, ma anche tra computer diversi. I virus informatici sono spesso programmati per eseguire azioni distruttive, come il danneggiamento o l'eliminazione dei dati. Più tempo un virus rimane nel computer senza essere rilevato, maggiore sarà il numero di file infetti che alla fine saranno presenti nel computer.
    * [Worm](https://it.wikipedia.org/wiki/Worm) I worm sono in genere considerati come una sotto serie dei virus informatici, ma con alcune differenze specifiche: Un worm è un programma informatico che si replica, ma senza infettare altri file. Il worm si installa una volta su un computer, quindi cerca il modo di infestare altri computer. Mentre un virus è una parte di codice che si aggiunge ai file esistenti, i worm sono file separati, standalone.
     4.
